import {
  isEmail,
  isLength,
  isAlpha,
  isLowercase,
  isUppercase
} from "validator";
import axios from "axios";

export function getRedirectPath({ type, avatar }) {
  // 根据用户信息 返回跳转地址
  // user.type /boss /genius
  // user.avatar /bossinfo /geniusinfo
  let url = type === "boss" ? "/boss" : "/genius";
  if (!avatar) {
    url += "info";
  }
  return url;
}

export function redirectToDashboard() {
  let url = "/dashboard/service";
  return url;
}

export function validateEmail(e) {
  if (!isEmail(e)) {
    return false;
  } else return true;
}

export function validatePassword(e) {
  if (
    isUppercase(e) ||
    isAlpha(e) ||
    isLowercase(e) ||
    !isLength(e, { min: 8, max: undefined })
  ) {
    return false;
  } else return true;
}

export const getJwt = () => {
  return localStorage.getItem("token");
};

export const getRole = () => {
  return localStorage.getItem("role");
};

export function getSuggestion(src, params) {
  const API_HOST = src;
  const data = axios
    .get(API_HOST, {
      params: {
        max: params.max,
        query: params.query,
        type: params.type,
        src: params.src
      }
    })
    .then(function(response) {
      if (response.status === 200 && response.data.count > 0) {
        return { code: 0, data: response.data };
      } else {
        return { code: 1, msg: "connect failed" };
      }
    })
    .catch(function(error) {
      return { code: 1, msg: "connect failed" };
    });
  return data;
}

export function getAccessToken(src, params) {
  const API_HOST = src;
  const data = axios
    .get(API_HOST, {
      params: {
        client_id: 'c442a75e',
        client_secret: '209a1a25c9666ee2178bd39f48d77d08',
        grant_type: 'client_credentials'
      }
    })
    .then(function(response) {
      if (response.status === 200 && response.data.count > 0) {
        return { code: 0, data: response.data };
      } else {
        return { code: 1, msg: "connect failed" };
      }
    })
    .catch(function(error) {
      return { code: 1, msg: "connect failed" };
    });
  return data;
}