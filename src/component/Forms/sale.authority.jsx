import React, { Component } from "react";
import { Form, Icon, Input, Button, Checkbox, DatePicker, Select, Tooltip } from "antd";
// import './soi.css';
const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
const InputGroup = Input.Group;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 11 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 13 }
  },
};

class CustomForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form:", values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        {/* // With goods being: */}
        <FormItem label="With goods being:" {...formItemLayout}>
          {getFieldDecorator("goods", {
            rules: [{ required: true, message: "Please input your username!" }]
          })(<TextArea className="code001-input-width" rows={4} />)}
        </FormItem>
        {/* // Exclusive authority period : */}
        <FormItem label="Exclusive authority period:" {...formItemLayout}>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(<Input className="code001-input-width" addonAfter="days" />)}
        </FormItem>
        {/* // Continuing authority period : */}
        <FormItem label="Continuing authority period:" {...formItemLayout}>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(<Input className="code001-input-width" addonAfter="days" />)}
        </FormItem>
        {/* // Auction date : */}
        <FormItem label="Auction date:" {...formItemLayout} style={{textAlign:'left'}}>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(<DatePicker />)}
        </FormItem>

        {/* // Time of auction: */}
        <FormItem label="Time of auction:" {...formItemLayout}  style={{textAlign:'left'}}>
        <InputGroup compact id="auctionTime">
              
              {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Select initialValue="am">
                <Option value="am">am</Option>
                <Option value="pm">pm</Option>
              </Select>
          )}
              {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Input className="code001-input-width" style={{ width: 120 }} />
          )}
            </InputGroup>
          
        </FormItem>

        {/* // Property is being sold: */}
        <FormItem label="Property is being sold:" {...formItemLayout}  style={{textAlign:'left'}}>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Checkbox.Group>
              <Checkbox value="A">with vacant possession or</Checkbox>
              <br />
              <Checkbox value="B">subject to a tenancy on payment of</Checkbox>
              <br />
              <Checkbox value="C">full purchase price</Checkbox>
            </Checkbox.Group>
          )}
        </FormItem>

        {/* // Upon terms on payment of */}
        <FormItem label="Upon terms on payment of :" {...formItemLayout}  style={{textAlign:'left'}}>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Checkbox.Group>
              <Checkbox value="A">full deposit</Checkbox>
              <br />
            </Checkbox.Group>
          )}
          {/* // the sum of: */}
            <FormItem >
            {getFieldDecorator("password", {
                rules: [{ required: true, message: "Please input your Password!" }]
            })(
                <Input className="code001-input-width" addonBefore="the sum of" />
            )}
            </FormItem>
        </FormItem>
        
        {/* // Property is being sold: */}
        <FormItem label="Vendor's Price :" {...formItemLayout}>
          {getFieldDecorator("vendorPrice", {
            rules: [{ required: true, message: "Please input your vendor's price!" }]
          })(
            <Input className="code001-input-width" />
          )}
        </FormItem>
         {/* // Property is being sold: */}
        <FormItem label="Payable in :" {...formItemLayout}>
          {getFieldDecorator("vendorPrice", {
            rules: [{ required: true, message: "Please input your vendor's price!" }]
          })(
            <Input className="code001-input-width" addonAfter="days"/>
          )}
        </FormItem>
          {/* // Property is being sold: */}
          <FormItem label="Agent's estimate price :" {...formItemLayout}>
          {getFieldDecorator("estimatePrice", {
            rules: [{ required: true, message: "Please input price!" }]
          })(
            <Input className="code001-input-width" />
          )}
        </FormItem>

        {/* // Property is being sold: */}
        <FormItem label="Agent's estimate price :" {...formItemLayout}>
         
            <InputGroup compact style={{ textAlign: "left" }} id="rangePrice">
            {getFieldDecorator("Minimum", {
            rules: [{ required: true, message: "Please input your vendor's price!" }]
          })(
            <Input
                      style={{ width: 100, textAlign: "center" }}
                      placeholder="Minimum"
                    />
          )}
                    
                    <Input
                      style={{
                        width: 30,
                        borderLeft: 0,
                        pointerEvents: "none",
                        backgroundColor: "#fff"
                      }}
                      placeholder="~"
                      disabled
                    />
                    {getFieldDecorator("max", {
            rules: [{ required: true, message: "Please input your vendor's price!" }]
          })(
            <Input
                      style={{ width: 100, textAlign: "center", borderLeft: 0 }}
                      placeholder="Maximum"
                    />
          )}
                    
                  </InputGroup>
        </FormItem>
      </Form>
    );
  }
}
const AuthorityForm = Form.create()(CustomForm);

export default function Authority() {
  return <AuthorityForm />;
}
