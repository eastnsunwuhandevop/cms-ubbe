import React, { Component } from "react";
import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Checkbox,
  Icon,
  Tooltip,
  message,
  Card,
  DatePicker,
  Select,
  Radio
} from "antd";
// import './soi.css';
import axios from "axios";
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
const InputGroup = Input.Group;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const { TextArea } = Input;
class code001 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      inputMinValue: 1,
      inputMaxValue: 1,
      commission: ""
    };
  }
  handleSubmit(e) {
    e.preventDefault();
    console.log(11111)
  }
  
  onChangeCommission(checkedValues) {
    this.setState({
      commission: checkedValues.target.value
    });
  }

  handleChange(value) {
    console.log(`Selected: ${value}`);
  }

  rendeFixedCommission() {
    return (
      <Row style={{ margin: "15px 0px" }}>
        <Col
          style={{
            lineHeight: "32px",
            textAlign: "left",
            padding: "0px 5px"
          }}
        >
          A fixed commission (including GST) of:
        </Col>
        <Col
          style={{
            textAlign: "left"
          }}
        >
          <Input
            className="code001-input-width"
            addonBefore="$"
            placeholder=""
          />
        </Col>
      </Row>
    );
  }

  renderEstimated() {
    return (
      <div>
        <Row style={{ margin: "15px 0px" }}>
          <Col
            style={{
              lineHeight: "32px",
              textAlign: "left",
              padding: "0px 5px"
            }}
          >
            A commission (including GST) being the following % of the sale
            price:
          </Col>
          <Col
            style={{
              textAlign: "left"
            }}
          >
            <Input
              className="code001-input-width"
              addonAfter="%"
              placeholder=""
            />
          </Col>
        </Row>
        <Row style={{ margin: "15px 0px" }}>
          <Col
            style={{
              lineHeight: "32px",
              textAlign: "left",
              padding: "0px 5px"
            }}
          >
            Dollar amount of estimated commission:
          </Col>
          <Col
            style={{
              textAlign: "left"
            }}
          >
            <Input className="code001-input-width" addonBefore="$" />
          </Col>
        </Row>
        <Row style={{ margin: "15px 0px" }}>
          <Col
            style={{
              lineHeight: "32px",
              textAlign: "left",
              padding: "0px 5px"
            }}
          >
            including GST:
          </Col>
          <Col
            style={{
              textAlign: "left"
            }}
          >
            <Input
              className="code001-input-width"
              addonBefore="$"
              placeholder="Single amount"
            />
          </Col>
        </Row>

        <Row style={{ margin: "15px 0px" }}>
          <Col
            style={{
              lineHeight: "32px",
              textAlign: "left",
              padding: "0px 5px"
            }}
          >
            if sold at a *GST inclusive/*GST exclusive price:
          </Col>
          <Col
            style={{
              textAlign: "left"
            }}
          >
            <Input
              className="code001-input-width"
              addonBefore="$"
              placeholder="Single amount"
            />
          </Col>
        </Row>
      </div>
    );
  }

  renderCommission() {
    const { commission } = this.state;
    if (commission === "fixed") {
      return this.rendeFixedCommission();
    }

    if (commission === "estimated") {
      return this.renderEstimated();
    }
  }

  render() {
    const { size } = this.state;
    return (
      <div className="soiContainer" style={{ width: "100%" }}>
        <form onSubmit={this.handleSubmit}>
          <Card
            title="Property"
            bordered={false}
            style={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <div style={{ maxWidth: 720 }}>
              <Row style={{ margin: "15px 0px" }}>
                <Col>
                  <Col
                    style={{
                      lineHeight: "32px",
                      textAlign: "left",
                      padding: "0px 5px"
                    }}
                  >
                    <h4>With goods being:</h4>
                  </Col>
                  <Col
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <TextArea id="goods" className="code001-input-width" rows={4} />
                  </Col>
                </Col>

                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Exclusive authority period :</h4>
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input id="exclusive" className="code001-input-width" addonAfter="days" />
                </Col>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Continuing authority period :</h4>
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input id="continuing" className="code001-input-width" addonAfter="days" />
                </Col>

                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Auction date :</h4>
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <DatePicker id="auctionDate"/>
                </Col>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Time of auction :</h4>
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <InputGroup compact id="auctionTime">
                    <Select defaultValue="1">
                      <Option value="1">am</Option>
                      <Option value="2">pm</Option>
                    </Select>
                    <Input
                      className="code001-input-width"
                      style={{ width: 200 }}
                    />
                  </InputGroup>
                </Col>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Property is being sold:</h4>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <Col
                    span={24}
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <Checkbox value="A" id="payment-1">with vacant possession or</Checkbox>
                  </Col>
                  <Col
                    span={24}
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <Checkbox value="B" id="payment-2">
                      subject to a tenancy on payment of
                    </Checkbox>
                  </Col>
                  <Col
                    span={24}
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <Checkbox value="C" id="payment-3">full purchase price</Checkbox>
                  </Col>
                </Col>
              </Row>
              <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
                <h5>OR</h5>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <h4>Upon terms on payment of :</h4>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                  <Col
                    span={24}
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <Checkbox value="A"  id="payment-4" style={{ marginBottom: 5 }}>
                      full deposit
                    </Checkbox>
                  </Col>
                  <Col
                    span={24}
                    style={{
                      textAlign: "left"
                    }}
                  >
                    <Checkbox value="A" style={{ marginBottom: 5 }}>
                      and the sum of
                    </Checkbox>
                    <Input style={{ width: 200 }} addonBefore="$"  id="payment-sum" />
                  </Col>
                </Col>
              </Row>
              <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
                <h4>Vendor's Price:</h4>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Single amount:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                   id="vendor-price"
                    className="code001-input-width"
                    addonBefore="$"
                    placeholder="Vendor’s price"
                  />
                </Col>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Payable in :
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                  id="payInDays"
                    className="code001-input-width"
                    addonAfter="days"
                    placeholder=""
                  />
                </Col>
              </Row>

              <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
                <h4>
                  <Tooltip
                    placement="rightTop"
                    title="Note: If a price range is specified, the difference between the upper and lower amounts cannot be more than 10% of the lower amount."
                  >
                    <Icon type="info-circle-o" /> Agent’s estimate of selling
                    price (Section 47A of the Estate Agents Act 1980)
                  </Tooltip>
                </h4>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Single amount:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                  id="agentPrice"
                    className="code001-input-width"
                    addonBefore="$"
                    placeholder="Single amount"
                  />
                </Col>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Range price between:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <InputGroup compact style={{ textAlign: "left" }} id="rangePrice">
                    <Input
                      style={{ width: 100, textAlign: "center" }}
                      placeholder="Minimum"
                    />
                    <Input
                      style={{
                        width: 30,
                        borderLeft: 0,
                        pointerEvents: "none",
                        backgroundColor: "#fff"
                      }}
                      placeholder="~"
                      disabled
                    />
                    <Input
                      style={{ width: 100, textAlign: "center", borderLeft: 0 }}
                      placeholder="Maximum"
                    />
                  </InputGroup>
                </Col>
              </Row>

              <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
                <h3>Commission</h3>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  <RadioGroup
                    onChange={v => this.onChangeCommission(v)}
                    defaultValue="a"
                  >
                    <RadioButton value="fixed">Fixed commission</RadioButton>
                    <RadioButton value="estimated">
                      Estimated commission
                    </RadioButton>
                  </RadioGroup>
                </Col>
              </Row>
              {this.renderCommission()}

              <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
                <h3>Marketing Expenses (including GST)</h3>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                 
                >
                  Advertising:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                   id="advertising"
                    className="code001-input-width"
                    addonBefore="$"
                    placeholder="Single amount"
                  />
                </Col>
              </Row>

              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Other Expenses:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                   id="otherExpenses"
                    className="code001-input-width"
                    addonBefore="$"
                    placeholder="Single amount"
                  />
                </Col>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  style={{
                    lineHeight: "32px",
                    textAlign: "left",
                    padding: "0px 5px"
                  }}
                >
                  Total:
                </Col>
                <Col
                  style={{
                    textAlign: "left"
                  }}
                >
                  <Input
                    className="code001-input-width"
                    addonBefore="$"
                    placeholder="Single amount"
                  />
                </Col>
              </Row>
              <Row style={{ margin: "15px 0px" }}>
                <Col
                  xs={24}
                  sm={8}
                  md={8}
                  lg={8}
                  xl={8}
                  style={{
                    lineHeight: "32px",
                    textAlign: "right",
                    padding: "0px 5px"
                  }}
                />
                <Col
                  xs={24}
                  sm={16}
                  md={16}
                  lg={16}
                  xl={16}
                  style={{ textAlign: "left" }}
                >
                  <Button type="dashed" style={{ marginRight: 8 }}>
                    Previous
                  </Button>
                  <Button type="submit" >Next</Button>
                </Col>
              </Row>
            </div>
          </Card>
        </form>
      </div>
    );
  }
}
const Code001Form = Form.create()(code001);

export default function Code001() {
  return <Code001Form />;
}
