import React, { Component } from "react";
import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Checkbox,
  Card,
  Slider,
  List,
  message,
  InputNumber,
  DatePicker,
  Select,
  Radio
} from "antd";
// import './soi.css';
import axios from "axios";
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
const InputGroup = Input.Group;
const Option = Select.Option;
const data = [
  {
    address: "Ant Design Title 1",
    price: "10000",
    date: "Date of sale"
  },
  {
    address: "Ant Design Title 2",
    price: "10000",
    date: "Date of sale"
  },
  {
    address: "Ant Design Title 3",
    price: "10000",
    date: "Date of sale"
  }
];

class soi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      inputMinValue: 1,
      inputMaxValue: 1
    };
  }

  render() {
    return (
      <div className="soiContainer" style={{ width: "100%" }}>
      <Row style={{ display: 'flex', justifyContent:'center' }}>
        <Col
              xs={24}
              sm={18}
              md={16}
              lg={12}
              xl={12}
            >
            <form>
        <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left"  }}>
            <h3>Indicative selling price</h3>
        </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
              Single:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16}>
              <Input placeholder="Indicative selling price" />
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
            Or between:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16}>
              <InputGroup compact style={{textAlign: 'left'}}>
               
                <Input
                  style={{ width: 100, textAlign: "center" }}
                  placeholder="Minimum"
                />
                <Input
                  style={{
                    width: 30,
                    borderLeft: 0,
                    pointerEvents: "none",
                    backgroundColor: "#fff"
                  }}
                  placeholder="~"
                  disabled
                />
                <Input
                  style={{ width: 100, textAlign: "center", borderLeft: 0 }}
                  placeholder="Maximum"
                />
              </InputGroup>
            </Col>
          </Row>
          <Row style={{ margin: "25px 0px 5px 0px", textAlign: "left" }}>
            <h3>Median sale price</h3>
        </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
              Median price:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16}>
              <Input placeholder="Median price" />
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
              Property type:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16} style={{textAlign: 'left'}}>
            <Radio.Group>
                <Radio.Button value="large">House</Radio.Button>
                <Radio.Button value="default">Unit</Radio.Button>
            </Radio.Group>
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
              Suburb or locality:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16} style={{textAlign: 'left'}}>
                <Input placeholder="Suburb or locality" />
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
            Period :
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16}>
              <InputGroup compact style={{textAlign: 'left'}}>
               
                <Input
                  style={{ width: 100, textAlign: "center" }}
                  placeholder="Minimum"
                />
                <Input
                  style={{
                    width: 30,
                    borderLeft: 0,
                    pointerEvents: "none",
                    backgroundColor: "#fff"
                  }}
                  placeholder="~"
                  disabled
                />
                <Input
                  style={{ width: 100, textAlign: "center", borderLeft: 0 }}
                  placeholder="Maximum"
                />
              </InputGroup>
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
              Source:
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16} style={{textAlign: 'left'}}>
                <Input placeholder="Source" />
            </Col>
          </Row>
          <Row style={{ margin: "15px 0px" }}>
            <Col
              xs={24}
              sm={8}
              md={8}
              lg={8}
              xl={8}
              style={{
                lineHeight: "32px",
                textAlign: "right",
                padding: "0px 5px"
              }}
            >
            </Col>
            <Col xs={24} sm={16} md={16} lg={16} xl={16} style={{textAlign: 'left'}}>
                <Button type="dashed" style={{ marginRight: 8}}>Previous</Button>
                <Button type="primary">Next</Button>
            </Col>
          </Row>
        </form>
            </Col>
        </Row>
      </div>
    );
  }
}
const SoiForm = Form.create()(soi);

export default function Soi() {
  return <SoiForm />;
}
