import React, { Component } from "react";
import { Steps, Button, message } from "antd";
import { doPrevious } from "../../redux/property.redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import AgentInfo from "../Property/agent.info";
import Soi from "../Forms/soi"
import Code001 from "../Forms/code001"
import Authority from "../Forms/sale.authority"
const Step = Steps.Step;
@connect(state => state.property,
{doPrevious}
)
class NewCaseSteps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0
    };
  }
  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }
  prev() {
    const current = this.props.current - 1;
    this.props.doPrevious(current)
    // this.setState({ current });
  }
  render() {
    const steps = [
      {
        title: "Property detail",
        content: Soi()
      },
      {
        title: "Statement of information",
        content: Soi()
      },
      {
        title: "Authority Form",
        content: "Second-content"
      },
      {
        title: "Agent info",
        content: "Last-content"
      }
    ];
    const { current } = this.props;
    return (
      <div>
        <div>{this.props.address ? this.props.address : null}</div>
        <Steps current={current}>
          {steps.map(item => <Step key={item.title} title={item.title} />)}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current === steps.length - 1 && (
            <Button
              type="primary"
              onClick={() => message.success("Processing complete!")}
            >
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(NewCaseSteps);
