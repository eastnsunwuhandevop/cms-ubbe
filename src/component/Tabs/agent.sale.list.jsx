import React, { Component } from 'react';
import {
    Tabs,
    Row,
    Col,
    Button,
    Spin,
    List,
    Card,
    Input
} from 'antd';
import './List.css';
import axios from 'axios'
import { withRouter } from 'react-router-dom';

const fakeDataUrl = 'http://192.168.0.107:3000/property/searchProperties';
const TabPane = Tabs.TabPane;
const Search = Input.Search;

class TableList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            loadingMore: false,
            showLoadingMore: true,
            data: [],
            id: "",
        }
        this.callback = this.callback.bind(this);
    }

    callback(key) {
        this.state.id = key;
        this.getData((res) => {
            if (res === 'error') {
                this.setState({
                    loading: false,
                });
            } else {
                this.setState({
                    loading: false,
                    data: res.data.data,
                });
            }
        });
    }
    action() {
        console.log("点了");
    }

    componentDidMount() {
        this.getData((res) => {
            if (res === 'error') {
                this.setState({
                    loading: false,
                });
            } else {
                this.setState({
                    loading: false,
                    data: res.data.data,
                });
            }
        });
    }
    getData = (callback) => {
        axios.post(fakeDataUrl, {
            identity_id: 1,
            property_type: this.state.id,
        }, { 'Content-Type': 'application/json' })
            .then(function (response) {
                callback(response);
            })
            .catch(function (error) {
                callback('error');
            });
    }
    onLoadMore = () => {
        this.setState({
            loadingMore: true,
        });
        this.getData((res) => {
            const data = this.state.data.concat(res.data.data);
            this.setState({
                data,
                loadingMore: false,
            }, () => {
                window.dispatchEvent(new Event('resize'));
            });
        });
    }

    renderAll() {
        const { loading, loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
            <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
                {loadingMore && <Spin />}
                {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
            </div>
        ) : null;
        return <List
            split="false"
            className="demo-loadmore-list"
            loading={loading}
            itemLayout="horizontal"
            loadMore={loadMore}
            dataSource={data}
            renderItem={item => (
                
                <List.Item style={{ marginLeft: '30px', marginRight: '30px' }}>
                    <List.Item.Meta
                        title={<a href="https://ant.design">{item.offered_forsal}</a>}
                    />
                    <Button className="SaleBtn" onClick={this.action}>Action</Button>
                </List.Item>
            )}
        />
    }


    render() {
        return (
                <Tabs defaultActiveKey="2" size="large" onChange={this.callback}>
                    <TabPane tab="All" key="1">{this.renderAll()}</TabPane>
                    <TabPane tab="Processing" key="2">{this.renderAll()}</TabPane>
                    <TabPane tab="Complete" key="3">{this.renderAll()}</TabPane>
                </Tabs>
            
        );
    }
}

export default withRouter(TableList);