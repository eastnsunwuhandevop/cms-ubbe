import React, { Component } from "react";
import { connect } from "react-redux";
import { doGetAddress } from "../../redux/property.redux";
import { AutoComplete, Button, Icon, Input } from "antd";
import { getSuggestion } from "../../utils/utils";

@connect(
    state => state.property,
    { doGetAddress }
)
class Search extends Component {
  state = {
    dataSource: [],
    selected:false,
    address:''
  };

  mapDatasource(e) {
    let data = [];
    e.map(v => {
      data = [...data, v.display.text];
    });
    return data;
  }
  handleSearch(e) {
    const {selected} = this.state

    selected? this.setState({selected:false}) : null

    const params = {
      max: 7,
      query: e,
      type: "address",
      src: "homepage"
    };
    getSuggestion(
      "https://suggest.realestate.com.au/consumer-suggest/suggestions",
      params
    )
      .then(res => {
        if (res.code === 0) {
          const data = this.mapDatasource(res.data._embedded.suggestions);
          this.setState({
            dataSource: data
          });
        }
      })
      .catch(err => {
        this.setState({
          error: err
        });
      });
  }

  onSubmit() {
    const {selected, address} = this.state
    selected? this.props.doGetAddress(address) : null
  }

  onSelect(value){
    this.setState({
      selected:true,
      address:value
    })
  }

  render() {
    const { dataSource } = this.state;
    return (
      <div className="global-search-wrapper .agent-info-from " style={{ width: 500, marginBottom:20 }}>
        <AutoComplete
          className="global-search"
          dataSource={dataSource}
          style={{ width: "100%" }}
          size="large"
          onSelect={(val) => this.onSelect(val)}
          onChange={(val) => this.handleSearch(val)}
          placeholder="Property address"
        >
          <Input
            suffix={
              <Button
                className="search-btn"
                size="large"
                type="primary"
                onClick={() => {
                  this.onSubmit();
                }}
              >
                {this.state.selected? <Icon type="arrow-right" /> : <Icon type="search" />}
              </Button>
            }
          />
        </AutoComplete>
      </div>
    );
  }
}

export default Search