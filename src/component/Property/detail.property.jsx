import React, { Component } from "react";
import { Card, Icon, Avatar } from 'antd';
import { getSuggestion } from "../../utils/utils";
const { Meta } = Card;
const listData = [{
    offered_forsal: '172-176 Old Dandenong Road',
    area: 'Heatherton Vic 3202',
    property_type: 'House',
    Bedrooms: '7',
    Bathrooms: '3',
    LandSize: '0.82ha(2.01 acres)(approx)',
    GarageSpace: '2',
}];

class Detail extends Component {
    renderCard(){
        return <Card
        style={{ width: 500, marginBottom:30 }}
        bordered={false}
        cover={<img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
    >
        <Meta
        title="172-176 Old Dandenong Road, Heatherton Vic 3202"
        />
        <div className="property-info-desc">
            <p>Property type: </p>
            <p>Bedrooms: </p>
            <p>Bathrooms: </p>
            <p>Land Size: </p>
            <p>Garage Space: </p>
        </div>
    </Card>
    }
  render() {
    return (
        <div>{this.renderCard()}</div>
        
    );
  }
}

export default Detail