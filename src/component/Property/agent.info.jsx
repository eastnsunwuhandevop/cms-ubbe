import React, { Component } from "react";
import { AutoComplete, Button, Icon, Input } from "antd";
import { doNext } from "../../redux/property.redux";
import { connect } from "react-redux";

const dataSource = ['Burns Bay Road', 'Downing Street', 'Wall Street'];
@connect(
    state => state.property,
    { doNext }
)
class AgentInfoForm extends Component {
    state = {
        dataSource: [],
        attention:this.props.attention
      };
    renderContent(){
        return <Input style={{width:'80%'}}placeholder="Please type your attention" />
    }

    onSelect(value){
        this.setState({
          attention: value
        })
      }

      onChange(value){
        this.setState({
          attention: value
        })
      }

      onSubmit() {
        const {attention} = this.state
        const data = {
            current: this.props.current + 1,
            attention: attention
        }
        if(attention){
            this.props.doNext(data)
        }
      }

  render() {
      const {attention} = this.props
    return (
        <div className="global-search-wrapper agent-info-from" style={{ width: 500, marginBottom:20 }}>
        <AutoComplete
          className="global-search"
          defaultValue={attention}
          dataSource={dataSource}
          style={{ width: "100%" }}
          size="large"
          onSearch={(val) => this.onChange(val)}
          onSelect={(val) => this.onSelect(val)}
          placeholder="Property address"
        >
          <Input
            suffix={
              <Button
                className="search-btn"
                size="large"
                type="primary"
                onClick={(e) => {
                  this.onSubmit(e);
                }}
              >
                <Icon type="arrow-right" />
              </Button>
            }
          />
        </AutoComplete>
      </div>
    );
  }
}

export default function AgentInfo() {
    return <AgentInfoForm />
  
  }