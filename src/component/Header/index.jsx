import React, { Component } from "react";
import { Layout, Menu, Icon, Dropdown } from "antd";
const { Header } = Layout;

const style={
    justifyContent: 'flex-end',
    display: 'flex',
    background: 'rgb(255, 255, 255)',
    padding: '0px 10px',
}
class DashboardHeader extends Component {
  renderDropDown() {
    const menu = (
      <Menu>
        <Menu.Item>
          <a rel="noopener noreferrer" href="/dashboard/profile">
            <Icon type="user" /> Profile
          </a>
        </Menu.Item>
        <Menu.Item>
          <a rel="noopener noreferrer" href="/dashboard/mailbox">
            <Icon type="message" /> Mailbox
          </a>
        </Menu.Item>
        <Menu.Item>
          <a rel="noopener noreferrer" href="">
            <Icon type="logout" /> Logout
          </a>
        </Menu.Item>
      </Menu>
    );

    return (
      <Dropdown overlay={menu}>
        {/* <a className="ant-dropdown-link" href="#">
          {this.props.mail} <Icon type="down" />
        </a> */}
        <a className="ant-dropdown-link" href="">
          yhx0634@gmail.com <Icon type="down" />
        </a>
      </Dropdown>
    );
  }
  render() {
    return (
      <Header style={{ ...style }}>
        {/* {this.props.mail ? this.renderDropDown() : null} */}
        {this.renderDropDown()}
      </Header>
    );
  }
}

export default DashboardHeader;
