import React, { Component } from "react";
import { Layout} from "antd";
import LeftSiderMenu from './menu'
const { Sider} = Layout;
class LeftSider extends Component {
  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }
  render(){
    return(
      <Sider
      breakpoint="lg"
      collapsedWidth="0"
      onCollapse={(collapsed, type) => {}}
    >
      <div className="logo" />
      <LeftSiderMenu />
    </Sider>
    );
  }
}

export default LeftSider
