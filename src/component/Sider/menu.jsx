import React, { Component } from "react";
import { Menu, Icon } from "antd";
import { withRouter } from "react-router-dom";

const SubMenu = Menu.SubMenu;
let defaultSelectedKey
// let defaultOpenKey
class LeftSiderMenu extends Component {
  onSelect = info => {
    console.log(info.key)
    this.props.history.push(info.key);
  };

  getdefaultOpenKey(){
    const pathname = this.props.location.pathname
    defaultSelectedKey = pathname
    if(pathname === '/dashboard/agent/sale/list' || pathname === '/dashboard/agent/sale/new' ){
        defaultSelectedKey = '/dashboard/agent/sale/list'
        return "agent"
    } else 
    if(pathname === '/dashboard/agent/lease/list' || pathname === '/dashboard/agent/lease/new' ){
        defaultSelectedKey = '/dashboard/agent/lease/list'
        return "agent"
    } else if(pathname === '/dashboard/agent/company/case' || pathname === '/dashboard/agent/company/member' || pathname === '/dashboard/agent/company/profile'){
        return "company"
    } else return null
  }

  render() {
    const defaultOpenKey = this.getdefaultOpenKey()
    return (
      <Menu
        theme="dark"
        mode="inline"
        inlineCollapsed = {false}
        defaultSelectedKeys={[defaultSelectedKey]}
        onClick={this.onSelect}
        onSelect={this.onSelect}
        defaultOpenKeys={[defaultOpenKey]}
      >
        <Menu.Item key="/dashboard/agent/report">
          <Icon type="dashboard" />
          <span className="nav-text">Dashboard</span>
        </Menu.Item>
        <SubMenu
          key="agent"
          title={
            <span>
              <Icon type="form" />
              <span>Agent</span>
            </span>
          }
        >
          <Menu.Item key="/dashboard/agent/sale/list">Sale</Menu.Item>
          <Menu.Item key="/dashboard/agent/lease/list">Lease</Menu.Item>
        </SubMenu>
        <SubMenu
          key="company"
          title={
            <span>
              <Icon type="team" />
              <span>Company</span>
            </span>
          }
        >
          <Menu.Item key="/dashboard/agent/company/case">Cases</Menu.Item>
          <Menu.Item key="/dashboard/agent/company/member">Member</Menu.Item>
          <Menu.Item key="/dashboard/agent/company/profile">
            Company profile
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="/dashboard/transaction">
        <Icon type="shop" />
          <span className="nav-text">Transaction</span>
        </Menu.Item>
        <Menu.Item key="/dashboard/mailbox">
         <Icon type="inbox" />
          <span className="nav-text">Mailbox</span>
        </Menu.Item>
        <Menu.Item key="/dashboard/profile">
          <Icon type="user" />
          <span className="nav-text">Profile</span>
        </Menu.Item>
      </Menu>
    );
  }
}

export default withRouter(LeftSiderMenu);
