import axios from 'axios'
import { redirectToDashboard} from '../utils/utils'
const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
const LOGIN_SUCESS = 'LOGIN_SUCESS'
const ERROR_MSG = 'ERROR_MSG'
const LOAD_DATA = 'LOAD_DATA'
const initState={
	redirectTo:'',
	isAuth:false,
	msg:'',
	user:'',
	type:''
}
// reducer
export function user(state=initState, action){
	switch(action.type){
		case REGISTER_SUCCESS:
			return {...state, msg:'',redirectTo:redirectToDashboard(),isAuth:true,...action.payload}
		case LOGIN_SUCESS:
			return {...state, msg:'',redirectTo:redirectToDashboard(),isAuth:true,...action.payload}
		case LOAD_DATA:
			return {...state, redirectTo:redirectToDashboard(), ...action.payload}
		case ERROR_MSG:
			return {...state, isAuth:false, msg:action.msg}
		default:
			return state
	}
} 

function registerSuccess(data){
	return { type:REGISTER_SUCCESS, payload:data}
}
function loginSuccess(data){
	localStorage.setItem('token', data.access_token)
	return { type:LOGIN_SUCESS , payload:data}
}
function errorMsg(msg){
    
	return { msg, type:ERROR_MSG }
}

export function loadData(userinfo){
	return { type:LOAD_DATA, payload:userinfo}
}

export function login(login_id,login_token){
	if (!login_id||!login_token) {
		return errorMsg('Email/Password invalid')
	}
	return dispatch=>{
		axios.post('/api/v1/client/login',{login_id,login_token})
			.then(res=>{
				if (res.status === 200&&res.data.code === 0) {
					dispatch(loginSuccess(res.data.data))
				}else{
					dispatch(errorMsg(res.data.message))
				}
			})		
	}
}

export function register(login_id,login_token){
	if (!login_id||!login_token) {
		return errorMsg('Email/Password invalid')
	}
	return dispatch=>{
		axios.post('/api/v1/client/register',{login_id,login_token})
			.then(res=>{
				if (res.status === 200&&res.data.code === 0) {
					dispatch(registerSuccess(res.data.data))
				}else{
					dispatch(errorMsg(res.data.message))
				}
			})		
	}
}

export function getProtectedData(token){
	return dispatch=>{
		axios.post('/api/v1/client/auth',{token})
			.then(res=>{
				if (res.status === 200&&res.data.code === 0) {
					
					dispatch(loginSuccess(res.data.data))
				}else{
					dispatch(errorMsg(res.data.message))
				}
			})		
	}
}