import axios from 'axios'
const GET_ADDRESS_SUCCESS = 'GET_ADDRESS_SUCCESS'
const STEP_NEXT = 'STEP_NEXT'
const STEP_PREVIOUS = 'STEP_PREVIOUS'

const initState={
	redirectTo:'',
    loaded:false,
    type:'',
	address:'',
	current: 0
}
// reducer
export function property(state=initState, action){
	switch(action.type){
		case GET_ADDRESS_SUCCESS:
			return {...state, loaded:true, redirectTo:'/login', address:action.payload}
		case STEP_NEXT:	
			return {...state, ...action.payload}
		case STEP_PREVIOUS:	
			return {...state, current: action.payload}
		default:
			return state
	}
} 

function getAddressSuccess(data){
	
	return { type:GET_ADDRESS_SUCCESS, payload:data}
}

function stepNext(data){
	return { type:STEP_NEXT, payload:data}
}

function previousNext(data){
	return { type:STEP_PREVIOUS, payload:data}
}
// function loginSuccess(data){
// 	localStorage.setItem('token', data.access_token)
// 	return { type:LOGIN_SUCESS , payload:data}
// }
// function errorMsg(msg){
    
// 	return { msg, type:ERROR_MSG }
// }

export function doGetAddress(property){
	console.log(property)
	return dispatch=>{
		dispatch(getAddressSuccess(property))	
	}
}

export function doNext(value){
	return dispatch=>{
		dispatch(stepNext(value))	
	}
}

export function doPrevious(value){
	return dispatch=>{
		dispatch(previousNext(value))	
	}
}
