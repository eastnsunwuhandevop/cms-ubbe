import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import style file
import "./index.css";
import "antd/dist/antd.css";
// import redux related file
import reducers from "./reducer";
// import component
import Login from "./container/Login";
import Register from "./container/Register";
import Dashboard from "./container/Dashboard";
import NotFoundPage from "./container/Error/404"
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(reducers, compose(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div className="main">
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route path="/" component={Dashboard} />
          {/* <AuthRoute>
            <Route path="/" component={Dashboard} />
          </AuthRoute> */}
          <Route component={NotFoundPage}></Route>
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
