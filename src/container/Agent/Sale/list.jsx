import React, { Component } from 'react';
import Tablist from '../../../component/Tabs/agent.sale.list'
import { withRouter } from "react-router-dom";
import { Button } from "antd";
class CaseList extends Component {
    add(){
        this.props.history.push('/dashboard/agent/sale/new')
    }
    render() {
        return (
         <div className="case-list">
            <div style={{ marginBottom: 16 }}>
                <Button  style={{color:'#1890ff'}}type="dashed" onClick={()=>{this.add()}}>CREATE CASE</Button>
            </div>
            <Tablist />
         </div>
        );
      }
}

export default withRouter(CaseList)