import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {Redirect} from 'react-router-dom'
import { connect } from "react-redux";
import Steps from '../../../component/Step/agent.sale.step'
import PropertySearch from '../../../component/Property/property'

@withRouter
@connect(
  state => state.property
)
class NewCase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  render() {
    return (
      <div className="new-case">
      {this.props.loaded? <Steps /> : PropertySearch()}
      {/* <Steps /> */}
      </div>
    );
  }
}

export default NewCase;
