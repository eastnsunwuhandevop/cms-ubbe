import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Layout} from "antd";

// import components
import SaleCaseList from "../Agent/Sale/list";
import SaleNewCase from "../Agent/Sale/new";
import LeaseCaseList from "../Agent/Lease/list";
import LeaseNewCase from "../Agent/Lease/new";
import CompanyCaseList from "../Agent/Company/list";
import CompanyMemberList from "../Agent/Company/member";
import CompanyProfile from "../Agent/Company/profile";
import Mailbox from "../Mailbox";
import Profile from "../Profile";
import Transaction from "../Transaction";
import Report from "../Agent/Report";
import LeftSider from "../../component/Sider/index";
import DashboardHeader from '../../component/Header'

import Authority from "../../component/Forms/sale.authority"

// import style file
import "./dashboard.css";
const { Content, Footer } = Layout;

class Dashboard extends Component {
  render() {
    return (
      <div className="dashboard">
        <Layout style={{ height:'100%' }}>
          <LeftSider />
          <Layout style={{ height:'100%' }}>
            <DashboardHeader />
            <Content style={{ margin: "24px 16px 0" }}>
              <div style={{ display:'flex',padding: 24, minHeight:360, background:'#ffffff',flexDirection:'column',justifyContent:'center' }}>
                <Switch>
                  {/* {this.props.role > 0 ? <Route path='/dashboard/cases' component={ServiceList}></Route> : null}
                        {this.props.role === 2 ? <Route path='/dashboard/member' component={Member}></Route> : null} */}
                  <Route
                    path="/dashboard/agent/sale/list"
                    component={SaleCaseList}
                  />
                  <Route
                    path="/dashboard/agent/sale/new"
                    component={SaleNewCase}
                  />
                  <Route
                    path="/dashboard/agent/lease/list"
                    component={LeaseCaseList}
                  />
                  <Route
                    path="/dashboard/agent/lease/new"
                    component={LeaseNewCase}
                  />
                  <Route path="/dashboard/agent/report" component={Report} />
                  <Route
                    path="/dashboard/agent/company/case"
                    component={CompanyCaseList}
                  />
                  <Route
                    path="/dashboard/agent/company/member"
                    component={CompanyMemberList}
                  />
                  <Route
                    path="/dashboard/agent/company/profile"
                    component={CompanyProfile}
                  />
                  <Route path="/dashboard/mailbox" component={Mailbox} />
                  <Route path="/dashboard/profile" component={Profile} />
                  <Route
                    path="/dashboard/transaction"
                    component={Transaction}
                  />
                  <Route component={Report}></Route>
                  <Route component={Mailbox}></Route>

                </Switch>
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
            UBBE ©2018 Created by Eastnsun
            </Footer>
          </Layout>
        </Layout>
      </div>
    );
  }
}
export default Dashboard;
