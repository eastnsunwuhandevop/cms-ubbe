import { combineReducers } from 'redux'
import { user } from './redux/user.redux'
import { property } from './redux/property.redux'

export default combineReducers({user, property})